import { StudyYear } from "../modules/course/types.js";
import { CourseSeason } from "./types.js";

export const validStudyYear = (value: number): value is StudyYear =>
  Number.isInteger(value) && value > 0 && value < 5;

export function toSeason(seasonStr: "Becна" | "Осінь" | "Літо"): CourseSeason
export function toSeason(seasonStr: any): CourseSeason | undefined
export function toSeason(seasonStr: any): CourseSeason | undefined {
  switch (seasonStr) {
    case "Весна":
      return CourseSeason.SPRING;
    case "Осінь":
      return CourseSeason.AUTUMN;
    case "Літо":
      return CourseSeason.SUMMER;
  }
}
