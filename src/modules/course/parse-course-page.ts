import { ICourse, StudyYear } from "./types.js";
import { Cheerio, load } from "cheerio";
import { Document, Element } from "domhandler";
import { CourseSeason, EducationLevel } from "../../common/types.js";
import { getElementsByTagName, textContent } from "domutils";
import { toSeason, validStudyYear } from "../../common/parsing-commons.js";

class CoursePageParsingError extends Error {}

function toEducationLevel(level: string): EducationLevel | undefined {
  switch (level) {
    case "Бакалавр":
      return EducationLevel.BACHELOR;
    case "Магістр":
      return EducationLevel.MASTER;
  }
}

const isDefined = <T>(value: T | undefined): value is T => value !== undefined;

function retrieveSeasons(seasonsTable: Cheerio<Element>): CourseSeason[] {
  return [...parseKeyValTable(seasonsTable).keys()].map(toSeason).filter(isDefined);
}

function parseKeyValTable(tableElement: Cheerio<Element>): Map<string, Element> {
  const map = new Map<string, Element>();
  for (const row of tableElement.find("> tr")) {
    const header = getElementsByTagName("th", row)[0];
    const value = getElementsByTagName("td", row)[0];
    if (!header) continue;
    if (!value) continue;
    const key = textContent(header).trim();
    if (!key) continue;
    map.set(key, value);
  }
  return map;
}

function retrieveCode(table: Map<string, Element>): number {
  const element = table.get("Код");
  if (!element) {
    throw new CoursePageParsingError("Cannot retrieve course code");
  }
  const code = parseInt(textContent(element), 10);
  if (Number.isNaN(code)) {
    throw new CoursePageParsingError("Code is presented in invalid format");
  }
  return code;
}

function retrieveFaculty(table: Map<string, Element>): string {
  const element = table.get("Факультет");
  if (!element) {
    throw new CoursePageParsingError("Cannot retrieve faculty name");
  }
  return textContent(element);
}

function retrieveDepartment(table: Map<string, Element>): string {
  const element = table.get("Кафедра");
  if (!element) {
    throw new CoursePageParsingError("Cannot retrieve department name");
  }
  return textContent(element);
}

function retrieveTeacherName(table: Map<string, Element>): string | undefined {
  const element = table.get("Викладач");
  return element && textContent(element);
}

function retrieveEducationLevel(table: Map<string, Element>): EducationLevel {
  const element = table.get("Освітній рівень");
  if (!element) {
    throw new CoursePageParsingError("Cannot retrieve education level");
  }
  const level = toEducationLevel(textContent(element));
  if (!level) {
    throw new CoursePageParsingError(
      `Cannot convert education level "${level}" to known values`
    );
  }
  return level;
}

function retrieveInformationBadges(table: Map<string, Element>): string[] {
  const element = table.get("Інформація");
  if (!element) {
    throw new CoursePageParsingError("Cannot retrieve information badges");
  }
  return getElementsByTagName("span", element).map(textContent);
}

function retrieveStudyYear(badges: string[]): StudyYear {
  const yearStr = badges.find((badge) => /^\d рік$/.test(badge));
  if (!yearStr) {
    throw new CoursePageParsingError("Cannot retrieve study year badge");
  }
  const year = parseInt(yearStr[0]!, 10);
  if (!validStudyYear(year)) {
    throw new CoursePageParsingError(`Unacceptable year ${year} out of range`);
  }
  return year;
}

function retrieveStudyHours(badges: string[]): number {
  const hoursStr = badges.find((badge) => /^\d+ год.$/.test(badge));
  if (!hoursStr) {
    throw new CoursePageParsingError("Cannot retrieve study hour badge");
  }
  return parseInt(hoursStr, 10);
}

function retrieveCredits(badges: string[]): number {
  const creditsStr = badges.find((badge) => /^\d+ кред.$/.test(badge));
  if (!creditsStr) {
    throw new CoursePageParsingError("Cannot retrieve credits badge");
  }
  return parseInt(creditsStr, 10);
}

function retrieveName(document: Cheerio<Document>): string {
  return document.find("head > title").text();
}

function retrieveDescription(document: Cheerio<Document>): string | undefined {
  const element = document.find("[id^=course-card--][id$=--info]")[0];

  return (
    element &&
    textContent(element)
      .trim()
      .split("\n")
      .map((s) => s.trim())
      .join(" ")
  );
}

export function parseCoursePage(htmlPage: string): ICourse {
  const $ = load(htmlPage);

  const seasonsTable = $("#w0 > table > tbody:nth-child(2)");
  const infoTable = parseKeyValTable($("#w0 > table > tbody:nth-child(1)"));
  const badges = retrieveInformationBadges(infoTable);

  return {
    code: retrieveCode(infoTable),
    name: retrieveName($.root()),
    description: retrieveDescription($.root()),
    facultyName: retrieveFaculty(infoTable),
    departmentName: retrieveDepartment(infoTable),
    level: retrieveEducationLevel(infoTable),
    year: retrieveStudyYear(badges),
    hoursAmount: retrieveStudyHours(badges),
    creditsAmount: retrieveCredits(badges),
    teacherName: retrieveTeacherName(infoTable),
    seasons: retrieveSeasons(seasonsTable),
  };
}
