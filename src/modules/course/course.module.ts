import { Module } from "@nestjs/common";
import { CourseController } from "./course.controller.js";
import UKMAFetchCoursePageService from "./fetch-course-page.service.js";
import RetrieveCourseService from "./retrieve-course.service.js";

@Module({
  imports: [],
  controllers: [CourseController],
  providers: [
    { provide: "FetchCoursePageService", useClass: UKMAFetchCoursePageService },
    RetrieveCourseService,
  ],
})
export class CourseModule {}
