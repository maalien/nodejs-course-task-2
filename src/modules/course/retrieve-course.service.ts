import { Inject, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { IFetchCoursePageService } from "./fetch-course-page.service.js";
import { parseCoursePage } from "./parse-course-page.js";

export default class RetrieveCourseService {
  constructor(
    @Inject("FetchCoursePageService") private coursePage: IFetchCoursePageService
  ) {}

  public async retrieveCourse(code: number) {
    const page = await this.coursePage.fetch(code).catch(rethrowAs500);
    if (!page) throw new NotFoundException(`Course ${code} cannot be found`);
    return parseCoursePage(page);
  }
}

function rethrowAs500(error: unknown) {
  const message = hasStringMessage(error) ? error.message : undefined;
  throw new InternalServerErrorException(message);
}

const hasStringMessage = (obj: unknown): obj is { message: string } =>
  !!obj &&
  typeof obj === "object" &&
  hasOwnProperty(obj, "message") &&
  typeof obj.message === "string";

const hasOwnProperty = <P extends PropertyKey>(
  obj: unknown,
  property: P
): obj is { [p in P]: unknown } => Object.prototype.hasOwnProperty.call(obj, property);
