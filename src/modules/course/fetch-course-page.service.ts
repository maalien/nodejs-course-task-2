import fetch from "node-fetch";

export interface IFetchCoursePageService {
  fetch(courseCode: number): Promise<string | null>
}

export default class UKMAFetchCoursePageService implements IFetchCoursePageService {
  public async fetch(courseCode: number): Promise<string | null> {
    const response = await fetch(`https://my.ukma.edu.ua/course/${courseCode}`);
    if (response.status === 404) return null;
    if (!response.ok)
      throw new Error(`Failed to fetch course ${courseCode} from my.ukma.edu.ua`);
    return response.text();
  }
}
