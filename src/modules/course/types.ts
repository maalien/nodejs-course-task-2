import { CourseSeason, EducationLevel } from '../../common/types';

export type StudyYear = 1 | 2 | 3 | 4

export interface ICourse {
  code: number;
  name: string;
  description?: string;
  facultyName: string;  // Назва факультету
  departmentName: string; // Назва кафедри
  level: EducationLevel;
  year: StudyYear;
  seasons: CourseSeason[];
  creditsAmount?: number;
  hoursAmount?: number;
  teacherName?: string;
}
