import { readFile } from "node:fs/promises";
import {
  containsBothInputAndOutput,
  courseEq,
  TestDataFiles,
  testFiles,
} from "./test-commons";
import { Test } from "@nestjs/testing";
import { CourseModule } from "../course.module";
import request from "supertest";
import { INestApplication } from "@nestjs/common";
import { ICourse } from "../types";

const testdir = new URL("./testdata/", import.meta.url);

let shouldFail = false;

async function fetchCoursePageFromTestdir(courseCode: number) {
  if (shouldFail) throw new Error("Failed to fetch");
  try {
    const filename = new URL(`${courseCode}.input.html`, testdir);
    return (await readFile(filename)).toString();
  } catch (e) {
    return null;
  }
}

let app: INestApplication;

beforeAll(async () => {
  const module = await Test.createTestingModule({
    imports: [CourseModule],
  })
    .overrideProvider("FetchCoursePageService")
    .useValue({ fetch: fetchCoursePageFromTestdir })
    .compile();
  app = module.createNestApplication();
  await app.init();
});

afterAll(() => app.close());

beforeEach(() => {
  shouldFail = false;
});

test("Fetching nonexistent course results in 404", async () => {
  const response = await request(app.getHttpServer()).get("/course/999999");
  expect(response.statusCode).toBe(404);
});

test("Fetching course with invalid (non-numeric) code should result in 400", async () => {
  const response = await request(app.getHttpServer()).get("/course/nope");
  expect(response.statusCode).toBe(400);
});

test("Fetching course while underlying request fails should result in 500", async () => {
  shouldFail = true;
  const response = await request(app.getHttpServer()).get("/course/209026");
  expect(response.statusCode).toBe(500);
});

const completeEntry = (
  entry: [string, Partial<TestDataFiles>]
): entry is [string, TestDataFiles] => containsBothInputAndOutput(entry[1]);

async function prepareTestData([code, files]: [string, TestDataFiles]) {
  const output = await readFile(new URL(files.output, testdir));
  return { code: +code, output: JSON.parse(output.toString()) as ICourse };
}

const testData = await Promise.all(
  [...testFiles.entries()].filter(completeEntry).map(prepareTestData)
);

for (const { code, output } of testData) {
  test(`Fetching test course ${code} results in described output`, async () => {
    const result = await request(app.getHttpServer()).get(`/course/${code}`);
    expect(result.ok).toBeTruthy();
    expect(result.headers["content-type"]).toContain("application/json");
    expect(result.body).toMatchObject(output);
    expect(courseEq(result.body, output)).toBeTruthy();
  });
}
