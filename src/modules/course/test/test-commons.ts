import { readdir } from "fs/promises";
import { ICourse } from "../types";

const testdataPath = new URL("./testdata/", import.meta.url);
const dir = await readdir(testdataPath);

export type TestDataFiles = { input: string; output: string };

export const testFiles = new Map<string, Partial<TestDataFiles>>();

const isTestFile = (filename: string) =>
  /.*\.(input|output)\..*/.test(filename);

for (const filename of dir.filter(isTestFile)) {
  const id = filename.split(".")[0]!;
  const entry = testFiles.get(id) || {};
  if (/\.input\./.test(filename)) {
    entry.input = filename;
  }
  if (/\.output\./.test(filename)) {
    entry.output = filename;
  }
  testFiles.set(id, entry);
}

export const containsBothInputAndOutput = (
  files: Partial<TestDataFiles>
): files is TestDataFiles => Boolean(files.input && files.output);

export const testDataFilepath = (filename: string) => new URL(filename, testdataPath);

export const courseEq = (a: ICourse, b: ICourse) =>
  a.code === b.code &&
  a.description === b.description &&
  a.name === b.name &&
  a.facultyName === b.facultyName &&
  a.departmentName === b.departmentName &&
  a.level === b.level &&
  a.year === b.year &&
  a.seasons.every((season) => b.seasons.includes(season)) &&
  b.seasons.every((season) => a.seasons.includes(season)) &&
  a.creditsAmount === b.creditsAmount &&
  a.hoursAmount === b.hoursAmount &&
  a.teacherName === b.teacherName;
