import { readFile } from "fs/promises";
import { containsBothInputAndOutput, courseEq, testDataFilepath, TestDataFiles, testFiles } from "./test-commons";
import { parseCoursePage } from "../parse-course-page";
import { ICourse } from "../types";

const readTestData = (files: TestDataFiles) =>
  Promise.all([
    readFile(testDataFilepath(files.input)),
    readFile(testDataFilepath(files.output)),
  ]).then(([input, output]) => ({
    input: {
      contents: input.toString(),
      filename: files.input,
    },
    output: JSON.parse(output.toString()) as ICourse,
  }));

const testData = await Promise.all(
  [...testFiles.values()].filter(containsBothInputAndOutput).map(readTestData)
);

for (const { input, output } of testData) {
  test(`Parsing ${input.filename} produces described output`, () => {
    const parsed = parseCoursePage(input.contents);
    expect(parsed).toMatchObject(output); // For visual indication
    expect(courseEq(parsed, output)).toBeTruthy()
  });
}
