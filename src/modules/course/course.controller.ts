import {
  BadRequestException,
  Controller,
  Get,
  Param,
  ParseIntPipe,
} from "@nestjs/common";
import RetrieveCourseService from "./retrieve-course.service.js";
import { ICourse } from "./types.js";

const parseCodePipe = new ParseIntPipe({
  exceptionFactory: () => new BadRequestException("Course code should be an integer"),
});

@Controller("course")
export class CourseController {
  constructor(private course: RetrieveCourseService) {}

  @Get(":code")
  public async getCourse(@Param("code", parseCodePipe) code: number): Promise<ICourse> {
    return await this.course.retrieveCourse(code);
  }
}
