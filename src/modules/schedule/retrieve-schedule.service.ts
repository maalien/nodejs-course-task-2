import { Inject, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { IFetchSchedulePageService } from "./fetch-schedule-page.service.js";
import { parseSchedulePage } from "./parse-schedule-page";
import { Season } from "./schedule-item.js";

export default class RetrieveScheduleService {
  constructor(
    @Inject("FetchSchedulePageService") private schedulePage: IFetchSchedulePageService
  ) {}

  async retrieveSchedule(year: number, season: Season) {
    const page = await this.schedulePage
      .fetch(convertYearSeasonFormat(year, season), season)
      .catch((e) => {
        const message = e instanceof Error ? e.message : undefined;
        throw new InternalServerErrorException(message);
      });
    if (!page)
      throw new NotFoundException(
        `Schedule for year ${year} and ${season} season cannot be found`
      );
    return parseSchedulePage(page);
  }
}

function convertYearSeasonFormat(year: number, season: Season): number {
  switch (season) {
    case Season.AUTUMN:
      return year;
    case Season.SPRING:
    case Season.SUMMER:
      return year - 1;
  }
}
