import { readFile } from "node:fs/promises";
import { Test } from "@nestjs/testing";
import { ScheduleModule } from "../schedule.module.js";
import request from "supertest";
import { INestApplication } from "@nestjs/common";
import { IScheduleItem, Season } from "../schedule-item.js";

const testdir = new URL("./testdata/", import.meta.url);

function seasonToNumber(season: Season): 1 | 2 | 3 {
  switch (season) {
    case Season.AUTUMN:
      return 1;
    case Season.SPRING:
      return 2;
    case Season.SUMMER:
      return 3;
  }
}

let shouldFail = false;

async function fetchSchedulePageFromTestDir(year: number, season: Season) {
  if (shouldFail) throw new Error("Failed to fetch");
  try {
    const filename = new URL(`${year}.${seasonToNumber(season)}.input.html`, testdir);
    return (await readFile(filename)).toString();
  } catch (e) {
    return null;
  }
}

let app: INestApplication;

beforeAll(async () => {
  const module = await Test.createTestingModule({
    imports: [ScheduleModule],
  })
    .overrideProvider("FetchSchedulePageService")
    .useValue({ fetch: fetchSchedulePageFromTestDir })
    .compile();
  app = module.createNestApplication();
  await app.init();
});

afterAll(() => app.close());

beforeEach(() => {
  shouldFail = false;
});

test("Fetching nonexistent course results in 404", async () => {
  const response = await request(app.getHttpServer()).get("/schedule/2077/summer");
  expect(response.statusCode).toBe(404);
});

test("Fetching course with invalid (non-numeric) year should result in 400", async () => {
  const response = await request(app.getHttpServer()).get("/schedule/nope/spring");
  expect(response.statusCode).toBe(400);
});

test("Fetching course with invalid (out of range) year should result in 400", async () => {
  const response = await request(app.getHttpServer()).get("/schedule/-1/spring");
  expect(response.statusCode).toBe(400);
});

test("Fetching course with invalid season should result in 400", async () => {
  const response = await request(app.getHttpServer()).get("/schedule/2022/nope");
  expect(response.statusCode).toBe(400);
});

test("Fetching course while underlying request fails should result in 500", async () => {
  shouldFail = true;
  const response = await request(app.getHttpServer()).get("/schedule/2021/summer");
  expect(response.statusCode).toBe(500);
});

type TestEntry = { year: number; season: Season; output: string };

const testingEntries: TestEntry[] = [
  { year: 2021, season: Season.SUMMER, output: "2020.3.output.json" },
  { year: 2021, season: Season.AUTUMN, output: "2021.1.output.json" },
  { year: 2022, season: Season.SPRING, output: "2021.2.output.json" },
];

async function prepareTestData(entry: TestEntry) {
  const output = await readFile(new URL(entry.output, testdir));
  return { ...entry, output: JSON.parse(output.toString()) as IScheduleItem[] };
}

const testData = await Promise.all(testingEntries.map(prepareTestData));

for (const { year, season, output } of testData) {
  test(`Fetching test schedule of ${year}/${season} results in described output`, async () => {
    const result = await request(app.getHttpServer()).get(`/schedule/${year}/${season}`);
    expect(result.ok).toBeTruthy();
    expect(result.headers["content-type"]).toContain("application/json");
    expect(result.body).toMatchObject(output);
  });
}
