import { readdir, readFile } from "fs/promises";
import { CourseSeason } from "../../../common/types";
import {
  parseSchedulePage,
  parseUploadedAt,
  parseSpecialityName,
  parseSeason,
} from "../parse-schedule-page";
import { IScheduleItem } from "../schedule-item";

const testdata = new URL("./testdata/", import.meta.url);

const dir = await readdir(testdata);

export type TestDataFiles = { input: string; output: string };

export const testFiles = new Map<string, Partial<TestDataFiles>>();

const isTestFile = (filename: string) =>
  /.*\.(input|output)\..*/.test(filename);

for (const filename of dir.filter(isTestFile)) {
  const [year, season] = filename.split(".");
  const id = `${year}.${season}`;
  const entry = testFiles.get(id) || {};
  if (/\.input\./.test(filename)) {
    entry.input = filename;
  }
  if (/\.output\./.test(filename)) {
    entry.output = filename;
  }
  testFiles.set(id, entry);
}

export const containsBothInputAndOutput = (
  files: Partial<TestDataFiles>
): files is TestDataFiles => Boolean(files.input && files.output);

export const testDataFilepath = (filename: string) =>
  new URL(filename, testdata);

const readTestData = (files: TestDataFiles) =>
  Promise.all([
    readFile(testDataFilepath(files.input)),
    readFile(testDataFilepath(files.output)),
  ]).then(([input, output]) => ({
    input: {
      contents: input.toString(),
      filename: files.input,
    },
    output: JSON.parse(output.toString()) as IScheduleItem[],
  }));

const testData = await Promise.all(
  [...testFiles.values()].filter(containsBothInputAndOutput).map(readTestData)
);

for (const { input, output } of testData) {
  test(`Parsing ${input.filename} produces described output`, async () => {
    const parsed = parseSchedulePage(input.contents);
    expect(parsed).toMatchObject(output);
  });
}

const linkTexts = [
  ["Історія БП-1 Весна 2021–2022.docx", "Історія"],
  [
    "Філологія (германські мови та літератури (переклад включно)) БП-1 Весна 2021–2022.docx",
    "Філологія (германські мови та літератури (переклад включно))",
  ],
  [
    "Інженерія програмного забезпечення МП-2 Весна 2021–2022.xlsx",
    "Інженерія програмного забезпечення",
  ],
] as const;

for (const [input, expected] of linkTexts) {
  test(`Parsing link text of "${input}" results in desired output of "${expected}"`, () => {
    expect(parseSpecialityName(input)).toBe(expected);
  });
}

const createdAts = [
  ["(оновлено: 06.12.2021 15:16:43)", "2021-12-06 15:16:43"],
  ["(оновлено: 11.01.2022 09:28:05)", "2022-01-11 09:28:05"],
  ["(оновлено: 07.12.2021 17:19:07)", "2021-12-07 17:19:07"],
] as const;

for (const [input, expected] of createdAts) {
  test(`Parsing created at text of "${input}" results in desired output of "${expected}"`, () => {
    expect(parseUploadedAt(input)).toBe(expected);
  });
}

const seasons = [
  ["Весна, 2021–2022 н.р.", CourseSeason.SPRING],
  ["Осінь, 2020–2021 н.р.", CourseSeason.AUTUMN],
  ["Літо, 2016–2017 н.р.", CourseSeason.SUMMER],
] as const;

for (const [input, expected] of seasons) {
  test(`Parsing season of "${input}" results in desired output of ${expected}`, () => {
    expect(parseSeason(input)).toBe(expected);
  });
}
