import {
  BadRequestException,
  Controller,
  Get,
  Param,
  ParseEnumPipe,
  PipeTransform,
} from "@nestjs/common";
import RetrieveScheduleService from "./retrieve-schedule.service.js";
import { IScheduleItem, Season } from "./schedule-item.js";

const parseSeasonPipe = new ParseEnumPipe(Season, {
  exceptionFactory: () =>
    new BadRequestException('Season could be one of: "spring", "summer", or "autumn"'),
});

const parseYearPipe: PipeTransform = {
  transform(input: string) {
    const year = Number.parseInt(input, 10);
    if (!Number.isSafeInteger(year) || year < 2000 || year > 2200) {
      throw new BadRequestException(
        "Course code should be an integer in range of 2000 to 2200"
      );
    }
    return year;
  },
};

@Controller("schedule")
export class ScheduleController {
  constructor(private schedule: RetrieveScheduleService) {}

  @Get(":year/:season")
  public async getCourse(
    @Param("year", parseYearPipe) year: number,
    @Param("season", parseSeasonPipe) season: Season
  ): Promise<IScheduleItem[]> {
    return await this.schedule.retrieveSchedule(year, season);
  }
}
