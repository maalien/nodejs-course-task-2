import { Module } from "@nestjs/common";
import UKMAFetchSchedulePageService from "./fetch-schedule-page.service.js";
import RetrieveScheduleService from "./retrieve-schedule.service.js";
import { ScheduleController } from "./schedule.controller.js";

@Module({
  imports: [],
  controllers: [ScheduleController],
  providers: [
    { provide: "FetchSchedulePageService", useClass: UKMAFetchSchedulePageService },
    RetrieveScheduleService,
  ],
})
export class ScheduleModule {}
