import { Provider } from "@nestjs/common";
import fetch from "node-fetch";
import { Season } from "./schedule-item.js";

export interface IFetchSchedulePageService {
  fetch(year: number, season: Season): Promise<string | null>;
}

export default class UKMAFetchSchedulePageService implements IFetchSchedulePageService {
  public async fetch(year: number, season: Season): Promise<string | null> {
    const response = await fetch(
      `https://my.ukma.edu.ua/schedule?year=${year}&season=${seasonToNumber(season)}`
    );
    if (response.status === 404) return null;
    if (!response.ok)
      throw new Error(
        `Failed to fetch schedule for ${season} of ${year} from my.ukma.edu.ua`
      );
    return response.text();
  }
}

function seasonToNumber(season: Season): 1 | 2 | 3 {
  switch (season) {
    case Season.AUTUMN:
      return 1;
    case Season.SPRING:
      return 2;
    case Season.SUMMER:
      return 3;
  }
}
