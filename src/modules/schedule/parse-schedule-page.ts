import { CheerioAPI, load } from "cheerio";
import cheerio from "./cheerio.cjs";
import { IScheduleItem } from "./schedule-item.js";
import { Element } from "domhandler";
import { textContent } from "domutils";
import { CourseSeason, EducationLevel } from "../../common/types.js";
import { toSeason, validStudyYear } from "../../common/parsing-commons.js";

export function parseSchedulePage(htmlPage: string): IScheduleItem[] {
  const $ = load(htmlPage);
  const season = retrieveSeason($);
  const panels = retrieveFacultyPanels($);

  return panels.flatMap(parseFacultyPanel).map((item) => ({ ...item, season }));
}

class SchedulePageParsingError extends Error {}

function retrieveSeason(document: CheerioAPI): CourseSeason {
  const subtitle = document("#client-app > div > div.page-header > h1 > small").text();
  return parseSeason(subtitle);
}

const seasonRegex = /(?<rawSeason>Осінь|Весна|Літо)/;

export function parseSeason(subtitle: string): CourseSeason {
  const match = seasonRegex.exec(subtitle);
  const season = toSeason(match?.groups?.rawSeason);
  if (!season) throw new SchedulePageParsingError("Could not parse course season");
  return season;
}

function retrieveFacultyPanels(document: CheerioAPI): Element[] {
  return document("#schedule-accordion > div.panel").toArray();
}

type FacultyPanelItem = Omit<IScheduleItem, "season">;

function parseFacultyPanel(panelElement: Element): FacultyPanelItem[] {
  const facultyName = retrievePanelHeading(panelElement);

  return retrieveSpecialityPanels(panelElement)
    .flatMap(parseSpecialityPanel)
    .map((item) => ({ ...item, facultyName }));
}

function retrieveSpecialityPanels(element: Element): Element[] {
  return cheerio(element)
    .find("> div.panel-collapse > .panel-body > .panel-group > .panel-default")
    .toArray();
}

function retrievePanelHeading(panelElement: Element): string {
  const [heading] = cheerio(panelElement).find("> div.panel-heading > h4 > a").toArray();
  if (!heading) {
    throw new SchedulePageParsingError("Cannot retrieve faculty name");
  }
  return textContent(heading).trim();
}

type SpecialityPanelItem = Omit<FacultyPanelItem, "facultyName">;

function parseSpecialityPanel(panelElement: Element): SpecialityPanelItem[] {
  const title = retrievePanelHeading(panelElement);
  const { level, year } = parseSpecialityHeading(title);

  return retrieveScheduleItems(panelElement)
    .map(parseScheduleItem)
    .map((item) => ({ ...item, level, year }));
}

const specialityHeadingRegex = /^(?<rawLevel>БП|МП), (?<rawYear>\d) рік навчання$/;

function parseSpecialityHeading(title: string) {
  const match = specialityHeadingRegex.exec(title);
  const { rawLevel, rawYear } = match?.groups ?? {};
  if (!rawLevel || !rawYear) {
    throw new SchedulePageParsingError("Invalid speciality heading format");
  }
  // Regex should've already checked that it's in range
  const level = toEducationLevel(rawLevel as "БП" | "МП");
  const year = Number.parseInt(rawYear, 10);
  if (!validStudyYear(year)) {
    throw new SchedulePageParsingError("Invalid study year");
  }
  return { level, year };
}

function retrieveScheduleItems(panelElement: Element) {
  return cheerio(panelElement).find("> .panel-collapse > ul > li").toArray();
}

function toEducationLevel(str: "БП" | "МП"): EducationLevel {
  switch (str) {
    case "БП":
      return EducationLevel.BACHELOR;
    case "МП":
      return EducationLevel.MASTER;
  }
}

type ParseScheduleItemResult = Omit<SpecialityPanelItem, "year" | "level">;

function parseScheduleItem(itemElement: Element): ParseScheduleItemResult {
  return {
    specialityName: retrieveSpecialityName(itemElement),
    url: retrieveUrl(itemElement),
    updatedAt: retrieveUpdatedAt(itemElement),
  };
}

export function retrieveSpecialityName(itemElement: Element): string {
  const linkText = cheerio(itemElement)
    .find('a[href][title="Завантажити"]:last-child')
    .text()
    .trim();
  return parseSpecialityName(linkText);
}

const specialityNameRegex = /^(?<specialityName>.*) (БП|МП)-\d.*$/;

export function parseSpecialityName(linkText: string): string {
  const match = specialityNameRegex.exec(linkText);
  const { specialityName } = match?.groups ?? {};
  if (!specialityName) {
    throw new SchedulePageParsingError(
      "Schedule download link does not follow specified format"
    );
  }
  return specialityName;
}

function retrieveUrl(itemElement: Element): string {
  return cheerio(itemElement)
    .find('a[href][title="Завантажити"]:last-child')
    .attr("href")!;
}

function retrieveUpdatedAt(itemElement: Element): string {
  return parseUploadedAt(
    cheerio(itemElement).find("span.small.text-muted").text().trim()
  );
}

const uploadedAtRegex =
  /\(оновлено: (?<day>\d\d).(?<month>\d\d).(?<year>\d\d\d\d) (?<time>\d\d:\d\d:\d\d)\)/;

export function parseUploadedAt(str: string): string {
  const match = uploadedAtRegex.exec(str);
  const { day, month, year, time } = match?.groups ?? {};
  if (!day || !month || !year || !time) {
    throw new SchedulePageParsingError(
      "Schedule uploadedAt field does not follow specified format"
    );
  }
  return `${year}-${month}-${day} ${time}`;
}
