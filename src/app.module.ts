import { Module } from '@nestjs/common';
import { CourseModule } from './modules/course/course.module.js';
import { ScheduleModule } from './modules/schedule/schedule.module.js';

@Module({
  imports: [CourseModule, ScheduleModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
